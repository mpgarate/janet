import time
from datetime import datetime
import colored
import settings
from x256 import x256

from events.Event import Event
from models.User import User


class UserChange(Event):
    expected_type = "user_change"
    _user = None

    @property
    def user(self):
        if not self._user:
            self._user = User.from_source_object(self)
        return self._user

    @property
    def diff(self):
        ...

    def __repr__(self):
        return f"{self.user.display_name} changed their profile"
