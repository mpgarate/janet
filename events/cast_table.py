# come on, we can do better. isn't this duplicating the
# funtionality of the __typename__ or whatever field on all Events?
# why is this here???

from .Message import Message
from .Hello import Hello
from .UserTyping import UserTyping
from .ReactionAdded import ReactionAdded
from .UserChange import UserChange

cast_table = {
    "message": Message,
    "hello": Hello,
    "user_typing": UserTyping,
    "reaction_added": ReactionAdded,
    "user_change": UserChange,
}
