from events.Event import Event


class ReplyableEvent(Event):
    def reply(self, message):
        send_message_args = (message, self.channel_id, self.thread_ts)
        self.send_message(*send_message_args)
        return message

    def reply_thread(self, message, channel_id=None, timestamp=None):
        if self.is_in_thread:
            response = self.reply(message)
            return response

        send_message_args = (
            message,
            (channel_id or self.channel_id),
            (timestamp or self.raw_timestamp),
        )
        print(send_message_args)
        response = self.send_message(*send_message_args)

        return response

    def reply_dm(self, message):
        self.send_message(message, self.sender.uid)

    # Both this and the next function are the default way Message works
    # At the moment I only have to override it on ReactionAdded
    @property
    def is_in_thread(self):
        thread_ts_exists = "thread_ts" in self.raw_line
        return thread_ts_exists

    @property
    def thread_ts(self):
        if self.is_in_thread:
            return self.raw_line["thread_ts"]

    @property
    def channel_id(self):
        # This fails for message edited events
        # and gets really bad with thread-parents that are commands
        if "channel" in self.raw_line:
            return self.raw_line["channel"]
        elif self._channel:
            return self._channel
