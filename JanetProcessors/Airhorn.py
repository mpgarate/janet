from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import on_reacts

import random


class Airhorn(JanetProcessor):
    airhorn_responses = [
        "BYA BYA BYA BYOWWWWWWWW",
        ":airhorn: :airhorn: :airhorn: :airhorn: :fire: BYOOWWW :airhorn: :fire: :party_wizard: :fast_parrot: :fire: WHAT UP DOG :fire: :party_wizard: :fast_parrot: :fire: :airhorn: BYOOWWW :fire: :airhorn: :airhorn: :airhorn: :airhorn:",
        ":parrotwave1: BODY :parrotwave2: ROLL :parrotwave3: INTO :parrotwave4: THE :parrotwave5: WEEKENDDD :parrotwave6: DDDddd :parrotwave7:"
        "https://i.imgur.com/K92dVoV.gif",
    ]

    @on_reacts("airhorn")
    def process(self):
        self.reply(random.choice(self.airhorn_responses))
