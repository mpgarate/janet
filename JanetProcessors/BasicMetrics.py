from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import entry_point, messages_only, allow_events, startswith_words
from log.metrics import Metric
import time

from events.Message import Message
from events.ReactionAdded import ReactionAdded


class EventReceived(JanetProcessor):
    @entry_point()
    def process(self):
        Metric("events_received", value=1).put()
        if hasattr(self.event, "expected_type") and self.event.expected_type:
            Metric(self.event.expected_type, value=1).put()
        else:
            Metric(self.event.raw_line["type"], value=1).put()
