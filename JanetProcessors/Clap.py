from .JanetProcessor import JanetProcessor
from .decorators import i_startswith_words


class Clap(JanetProcessor):
    @i_startswith_words("?clap", "?claptext")
    def process(self):
        self.reply(" :clap: ".join(self.args) + " :clap:")
