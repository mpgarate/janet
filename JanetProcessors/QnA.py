from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import i_startswith_words, on_reacts

import requests
import random


class AskABook(JanetProcessor):
    url = "https://books.google.com/talktobooks/search_books"

    def get_book_responses(self, query):
        templated_data = f'["${query}",[5,null],null,[[]]]'
        query_payload = bytes(templated_data, "utf-8")

        response = requests.post(self.url, data=query_payload)
        response.raise_for_status()
        response_obj = response.json()
        return response_obj

    def send_book(self, book):
        book_name = book[1][3]
        text_response = self.extract_text_from_book(book)

        self.reply(f"> {text_response}")
        self.reply(f"from {book_name}")

    def extract_text_from_book(self, book):
        text_fields = [field for field in book[0] if type(field) is str]
        recombined_text = " ".join(text_fields[1:-2])
        return recombined_text

    @i_startswith_words("?ask")
    def ask(self):
        search_query = " ".join(self.args)

        book_responses = self.get_book_responses(search_query)

        book = book_responses[0][0]
        self.send_book(book)

    @i_startswith_words("?randomask")
    def randomask(self):
        search_query = " ".join(self.args)

        book_responses = self.get_book_responses(search_query)

        book = random.choice(book_responses[0])
        self.send_book(book)

#    @on_reacts("question")
#    def askmoji(self):
#        search_query = " ".join(self.args)
#
#        book_responses = self.get_book_responses(search_query)
#
#        book = random.choice(book_responses[0])
#        self.send_book(book)
