from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute
from JanetNeuron import JanetNeuron


class EmojiRequest(Model):
    class Meta:
        table_name = f"{JanetNeuron().workspace_name}_emoji_requests"

    emoji_name = UnicodeAttribute(hash_key=True)
    requester_id = UnicodeAttribute()
    description = UnicodeAttribute(null=True)

    def __repr__(self):
        return self.emoji_name


if not EmojiRequest.exists():
    EmojiRequest.create_table(read_capacity_units=1, write_capacity_units=1)
