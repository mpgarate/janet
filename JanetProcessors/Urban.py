from .JanetProcessor import JanetProcessor
from .decorators import startswith_words

import requests


class UrbanDict(JanetProcessor):
    urbandict_api_url = "http://api.urbandictionary.com/v0/define"
    # get param is "term" probably urlencoded

    @startswith_words("?urban", "?urbandict")
    def process(self):
        response = requests.get(self.urbandict_api_url, params={"term": " ".join(self.args)})
        if "list" in response.json() and response.json()["list"]:
            list_of_definitions = response.json()["list"]

        self.reply(response.json()["list"][0]["permalink"])
