from .JanetProcessor import JanetProcessor
from .decorators import startswith_words
from events.Message import Message
from events.Hello import Hello
import time
import importlib
import requests
import gevent
import os


class GDQ(JanetProcessor):
    gdq_triggers = ("?agdq", "?gdq", "?sgdq")

    @startswith_words(*gdq_triggers)
    def process(self):
        self.reply("https://www.twitch.tv/gamesdonequick")
